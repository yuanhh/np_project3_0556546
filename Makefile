RAS_ROOT:=$(shell echo $$HOME)/np/np_project3_0556546/
CC := gcc
CFLAGS := -g -Wall

all: http_server hw3.cgi

hw3.cgi: hw3.o socket_conn.h
	$(CC) $(CFLAG) socket_conn.o hw3.o -o hw3.cgi 

hw3.o: hw3.c socket_conn.h
	$(CC) $(CFLAG) -c hw3.c

http_server: http_server.o rwg_service.o socket_conn.o
	$(CC) $(CFLAG)  http_server.o rwg_service.o socket_conn.o -o http_server

http_server.o: http_server.c socket_conn.h rwg_service.h
	$(CC) $(CFLAG) -c http_server.c

rwg_service.o: rwg_service.c rwg_service.h
	$(CC) $(CFLAG) '-DRAS_ROOT="$(abspath $(RAS_ROOT))"' -c rwg_service.c

socket_conn.o: socket_conn.c socket_conn.h
	$(CC) $(CFLAG) -c socket_conn.c

.PHONY : clean
clean:
	-rm -f *.o http_server hw3.cgi
